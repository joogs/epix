FROM mosaicgreifswald/wildfly:23.0.2.Final-20210603



ENV EPIX_DB_HOST=mysql
ENV EPIX_DB_PORT=3306
ENV EPIX_DB_NAME=epix
ENV EPIX_DB_USER=epix_user
ENV EPIX_DB_PASS=epix_password

ENV JAVA_OPTS="-server -Xms1G -Xmx3G -XX:MetaspaceSize=256M -XX:MaxMetaspaceSize=1G -XX:StringTableSize=1000003 -Dorg.apache.cxf.stax.maxChildElements=50000 -Djava.net.preferIPv4Stack=true -Djava.awt.headless=true -Djboss.modules.system.pkgs=org.jboss.byteman"

ADD epix/epix-2.12.0.ear /entrypoint-wildfly-deployments/epix-2.12.0.ear
ADD epix/epix-web-2.12.0.war /entrypoint-wildfly-deployments/epix-web-2.12.0.war
ADD epix/configure_wildfly_epix.cli /entrypoint-wildfly-cli/configure_wildfly_epix.cli
ADD epix/keycloak_web.xml /entrypoint-wildfly-cli/keycloak_web.xml

ADD epix/start.sh start.sh

EXPOSE 8080

ENTRYPOINT [ "/bin/bash" ]
CMD [ "./start.sh" ]