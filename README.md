# EPIX in Docker

This repository contains the Dockerfile to build a dockerized version of [E-PIX](https://www.ths-greifswald.de/en/researchers-general-public/e-pix/) from the THS Greifswald.
The E-PIX source code is licensed under the AGPL-3.0 license and therefore, since this repository contains a redistribution of the original packaged software, this repository is subject to the license terms.

## Usage

The Docker image that can be built using the Dockerfile in this repository is meant for use in CI/CD workflows.
For it to work with the default environment variables, you need to create a MySQL container in the same bridge network as the E-PIX container.
Its hostname must be `mysql`.
It must listen on port 3306 and a new user must be created called `epix_user` with the password `epix_password`.
The user must have all privileges on a database called `epix` containing the schema that you can find in the original source distribution.

Do **not** use this image with default variables in production.
The default credentials are not safe for use.